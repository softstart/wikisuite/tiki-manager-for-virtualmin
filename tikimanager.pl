our (%in, %config);

use warnings;

sub script_tikimanager_desc
{
    return "Tiki Manager";
}

sub script_tikimanager_uses
{
    return ("php");
}

sub script_tikimanager_longdesc
{
    return "Tiki Manager permits you to manage various instances of Tiki."
        . " You can install, upgrade, backup, clone, check the file" 
        . " integrity and do various other things. ";
}

sub script_tikimanager_versions
{
    return ( "master" );
}

sub script_tikimanager_php_modules
{
    return ("bz2", "json", "mysql", "sqlite3", "zip");
}

sub script_tikimanager_can_upgrade
{
    return 1;
}

sub script_tikimanager_release
{
    return 1;
}

sub script_tikimanager_gpl
{
    return 1;
}

sub script_tikimanager_category
{
    return "Network";
}

sub script_tikimanager_site
{
    return 'https://gitlab.com/tikiwiki/tiki-manager';
}

sub script_tikimanager_depends
{
    local ($d, $ver) = @_;
    local @rv;

    my $php = &has_command($config{"php_cmd"} || "php");
    $php || push(@rv, "The php command is not installed");

    my $git = &has_command($config{"git_cmd"} || "git");
    $git || push(@rv, "The git command is not installed");

    my $sshkeygen = &has_command($config{"sshkeygen_cmd"} || "ssh-keygen");
    $sshkeygen || push(@rv, "The ssh-keygen command is not installed");

    return @rv;
}

sub script_tikimanager_params
{
    
}

sub script_tikimanager_parse
{
    local ($d, $ver, $in, $upgrade) = @_;

    if ($upgrade) {
        return $upgrade->{'opts'};
    }

}

sub script_tikimanager_check
{
    local ($d, $ver, $opts, $upgrade) = @_;
    my $rv;

    return $rv;
}

sub script_tikimanager_files
{
    
}

sub script_tikimanager_install
{
    eval "use LWP::Simple";
    local ($d, $version, $opts, $files, $upgrade, $domuser, $dompass) = @_;
    local ($out, $ex);

    my $curl = &has_command($config{"curl_cmd"} || "curl");
    my $git = &has_command($config{"git_cmd"} || "git");
    my $php = &has_command($config{"php_cmd"} || "php");
    my $sshkeygen = &has_command($config{"sshkeygen_cmd"} || "ssh-keygen");

    my $composer_url = "https://getcomposer.org/composer-2.phar";
    my $tiki_manager_url = "https://gitlab.com/tikiwiki/tiki-manager.git";

    my $tiki_manager_dir = "$d->{'home'}/tiki-manager";

    if (! -d "$tiki_manager_dir") {
        &run_as_domain_user($d, "$git clone '$tiki_manager_url' '$tiki_manager_dir'", 0);
    }

    if (! -f "$tiki_manager_dir/data/id_rsa") {
        &run_as_domain_user($d, "$sshkeygen -t rsa -N '' -f $tiki_manager_dir/data/id_rsa", 0);
    }

    # Running tiki Manager will make sure bot composer.phar is available and vendor is installed.
    $out = &run_as_domain_user($d,
        "cd $tiki_manager_dir && $php 'tiki-manager.php'"
            . " list"
            . " 2>&1",
        0
    );

    if (! -d "$tiki_manager_dir/vendor") {
        return (0, "Failed to install: $out")
    }

    return (1,
        "Tiki manager is now installed in the root folder of your account in the folder <pre>$tiki_manager_dir</pre> "
    );
}

sub script_tikimanager_uninstall
{
    eval "use File::Path;";
    local ($d, $version, $opts) = @_;

    my $tiki_manager_dir = "$d->{'home'}/tiki-manager";
    my $web_manager_dir = "$d->{'home'}/public_html";

    rmtree(@folders, {
        error => \my $err,
        safe => 1
    });

    if ($err && @$err) {
        my $message = "Error removing '$web_manager_dir': \n";

        for my $diag (@$err) {
            my ($file, $message) = %$diag;
            if ($file eq '') {
                $message .= "general error: $message\n";
            }
            else {
                $message .= "problem unlinking $file: $message\n";
            }
        }

        return (0, $message);
    }

    return (1,
        "The content of '$web_manager_dir' was successfully removed. However, "
        . " remaining files still exist on '$tiki_manager_dir'."
    );
}
