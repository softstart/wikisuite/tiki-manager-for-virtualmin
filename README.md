# Tiki Manager for Virtualmin

This script installs  [Tiki Manager](https://doc.tiki.org/Manager) (including the web interface) to manage Tiki Wiki CMS Groupware instances. 

It is the standard Tiki  Manager without any particular enhancements for Virtualmin. This is more for developers. You probably should be using https://gitlab.com/wikisuite/wikisuite-packages which will install https://gitlab.com/wikisuite/virtualmin-tikimanager

## Getting dependencies

```sh
sudo apt-get install -y \
    curl                \
    git-core            \
    openssh-client      \
    php-bz2             \
    php-json            \
    php-pdo
```

## Installing the Virtualmin addon

Just get the Perl file and drop it in the Virtualmin scripts folder, like below:

```sh
sudo curl -s -L \
    'https://gitlab.com/wikisuite/tiki-manager-for-virtualmin/-/raw/master/tikimanager.pl' \
    -o /usr/share/webmin/virtual-server/scripts/tikimanager.pl
```
